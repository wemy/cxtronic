package ml.wemy.cxtronic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ml.wemy.cxtronic.entity.Cedula;
import ml.wemy.cxtronic.enumerator.CedulaEnum;

@Repository
public interface CedulaRepository extends JpaRepository<Cedula,Long> {
	
	 Cedula findBycedula(CedulaEnum cedula);
	 List<Cedula> findAllByOrderByCedulaDesc();
	
}
