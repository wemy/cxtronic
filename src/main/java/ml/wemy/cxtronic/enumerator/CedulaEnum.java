package ml.wemy.cxtronic.enumerator;

import java.math.BigDecimal;

public enum CedulaEnum {
	
	DOIS      (2   , new BigDecimal(2.0)   ,"Dois"),
	CINCO     (5   , new BigDecimal(5.0)   ,"Cinco"),
	DEZ       (10  , new BigDecimal(10.0)  ,"Dez"),
	VINTE     (20  , new BigDecimal(20.0)  ,"Vinte"),
	CINQUENTA (50  , new BigDecimal(50.0)  ,"Cinquenta"),
	CEM       (100 , new BigDecimal(100.0) ,"Cem");
	
	private int tipoCedula;
	private BigDecimal valorCedula;
	private String descricaoCedula;
	
	CedulaEnum(int tipo, BigDecimal valor, String descricao) {
		tipoCedula = tipo;
		valorCedula = valor;
		descricaoCedula = descricao;
	}
	
	public int getTipo() {
		return tipoCedula;
	}
	
	public BigDecimal getValor() {
		return valorCedula;
	}
	
	public String getDescricao() {
		return descricaoCedula;
	}
	
}
