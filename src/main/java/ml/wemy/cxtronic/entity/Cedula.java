package ml.wemy.cxtronic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import ml.wemy.cxtronic.enumerator.CedulaEnum;

@Data
@Entity
public class Cedula {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private CedulaEnum cedula;
	
	@Column
	private int quantidade;
	
	public Cedula(){}
	
	public Cedula(CedulaEnum cedula, int quantidade){
		this.cedula = cedula;
		this.quantidade = quantidade;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CedulaEnum getCedula() {
		return cedula;
	}

	public void setCedula(CedulaEnum cedula) {
		this.cedula = cedula;
	}

	public int getQuantidade() {
		return quantidade;
	}
	
	public void somarQuantidade(int quantidade) {
		this.quantidade += quantidade;
	}
	
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
}
