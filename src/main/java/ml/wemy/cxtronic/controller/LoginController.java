package ml.wemy.cxtronic.controller;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value="session")
@Component(value="loginController")
@ELBeanName(value="loginController")
@Join(path="/authenticate", to="login-fb.jsf")
public class LoginController {

}
