package ml.wemy.cxtronic.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ml.wemy.cxtronic.entity.Cedula;
import ml.wemy.cxtronic.repository.CedulaRepository;

@Scope(value="session")
@Component(value="caixaController")
@ELBeanName(value="caixaController")
@Join(path="/caixa", to="index.jsf")
public class CaixaController {
	
	@Autowired
	private CedulaRepository cedulaRepository;
	
	private List<Cedula> cedulas;
	private BigDecimal valorSaque;
	private Cedula cedulaReabastecer = new Cedula();
	private List<List<Cedula>> listOpc = new ArrayList<>();

	@Deferred
	@RequestAction
	@IgnorePostback
	public void loadData() {
		cedulas = cedulaRepository.findAll();
	}
	
	@SuppressWarnings("unchecked")
	public void opcoesSaque(){
		opcoes(valorSaque);
	}
	
	@SuppressWarnings("unchecked")
	public void opcoes(BigDecimal valorSaque, List<Cedula>... args ) {
		
		List<Cedula> listCaixa = new ArrayList<>();
		List<Cedula> listPossiveis = new ArrayList<>();
		
		if(args != null && args.length > 0 ){
			listCaixa = args[0];
			if(args.length > 1 )
				listPossiveis = args[1];
		}else {
			listCaixa = cedulaRepository.findAllByOrderByCedulaDesc();
		}
		
		for (Cedula cedula : listCaixa) {
			for (int i = 1; i <= cedula.getQuantidade(); i++) {
				
				BigDecimal valorCedula = cedula.getCedula().getValor(); 
				
				if(valorCedula.compareTo(valorSaque) > 0 || valorCedula.multiply(new BigDecimal(i)).compareTo(valorSaque) > 0 )
					continue;
				
				Cedula aux = new Cedula(cedula.getCedula(), i);
				listPossiveis.add(aux);

				if(valorSaque.subtract(valorCedula.multiply(new BigDecimal(i))).compareTo(new BigDecimal(0)) == 0) {
					listOpc.add(new ArrayList<>(listPossiveis));
				}else {
					opcoes(valorSaque.subtract(valorCedula.multiply(new BigDecimal(i))), newListCedulaPop(listCaixa, cedula), listPossiveis);
				}

				listPossiveis.remove(aux);
				
				
			}
		}
	}

	public String reabastecerCedula(){
		
		Cedula dbCedula = cedulaRepository.findBycedula(cedulaReabastecer.getCedula());
		
		if(dbCedula != null) {
			dbCedula.somarQuantidade(cedulaReabastecer.getQuantidade());
			cedulaReabastecer = dbCedula;
		}
		
		cedulaRepository.save(cedulaReabastecer);
		cedulaReabastecer = new Cedula();
		loadData();
		return "/index.xhtml";
	}
	
	private List<Cedula> newListCedulaPut(List<Cedula> list ,Cedula cedula){
		
		List<Cedula> listPut = new ArrayList<>(list);
		listPut.add(cedula);
		
		return listPut;
	}
	
	private List<Cedula> newListCedulaPop(List<Cedula> list ,Cedula cedula){
		
		List<Cedula> listPop = new ArrayList<>(list);
		listPop.remove(cedula);
		
		return listPop;
	}
	
	public List<Cedula> getCedulas() {
		return cedulas;
	}

	public BigDecimal getValorSaque() {
		return valorSaque;
	}

	public void setValorSaque(BigDecimal valorSaque) {
		this.valorSaque = valorSaque;
	}
	
	public Cedula getCedulaReabastecer() {
		return cedulaReabastecer;
	}

	public List<List<Cedula>> getListOpc() {
		return listOpc;
	}
	
	
}
