package ml.wemy.cxtronic.controller;

import javax.annotation.ManagedBean;

import org.springframework.web.context.annotation.SessionScope;

@ManagedBean
@SessionScope
public class NavigationManagedBean {
	
	private String pagina = "reabastecer.xhtml";
	private boolean menuCashOutIsVisible = true;
	private boolean choiceCashIsVisible = false;
	private boolean cashOutIsVisible = false;
	
	public void choiceCash() {
		System.err.println("choiceCash");
		menuCashOutIsVisible = false;
		choiceCashIsVisible = true;
		cashOutIsVisible = false;
	}
	
	public void cashOut() {
		menuCashOutIsVisible = false;
		choiceCashIsVisible = false;
		cashOutIsVisible = true;
	}

	public void reabastecer() {
		this.pagina = "reabastecer.xhtml";
	}
	
	public void saque() {
		menuCashOutIsVisible = true;
		choiceCashIsVisible = false;
		cashOutIsVisible = false;
		this.pagina = "saque.xhtml";
	}
	
	//	GETTERS AND SETTERS
	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
	
	public boolean isMenuCashOutIsVisible() {
		return menuCashOutIsVisible;
	}
	public void setMenuCashOutIsVisible(boolean menuCashOutIsVisible) {
		this.menuCashOutIsVisible = menuCashOutIsVisible;
	}
	public boolean isChoiceCashIsVisible() {
		return choiceCashIsVisible;
	}
	public void setChoiceCashIsVisible(boolean choiceCashIsVisible) {
		this.choiceCashIsVisible = choiceCashIsVisible;
	}
	public boolean isCashOutIsVisible() {
		return cashOutIsVisible;
	}
	public void setCashOutIsVisible(boolean cashOutIsVisible) {
		this.cashOutIsVisible = cashOutIsVisible;
	}
	
}
