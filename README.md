o problema da melhor combinação de cedulas foi resolvido de maneira bem simples, a ideia é fazer a combinação das cedulas algo parecido com algoritmos de rede neural. onde todos os elementos possuem ligaçoes com os outros, onde cada cedula será multiplicada por possiveis quantidades e será somada pelas proximas combinações onde o mesmo processo anterior deve ser feito.

EXEMPLO

* cédulas disponiveis 
    
    DOIS = 10
    CINCO = 9
    DEZ = 0
    VINTE = 0
    CINQUENTA = 2
    CEM = 1

* valor solicitado 152,00

então o algoritmo pega o primeiro elemento da lista de cedulas disponiveis e começa multiplicando o valor da cédula por 1 até a quantidade de cedula disponivel.

a cada interação de multiplicação recursivamente ele fará o mesmo papel anterior na lista de cedulas disponiveis sem a cédula da interação anterior.

com algumas validações pode acelerar o processo 

- verificar se o valor requerido já foi atendido 
- se a cedula ou a multiplicação (cedulaXquantidade) tem o valor maior que o valor desejado 
- se a interação já está em um nivel maior que 3.

- se na lista cominações não existe outra combinação igual   

assim teriamos esse resultado.

(1 x 100,00 + 1 x 50,00  + 1 x 2,00   ) = 152,00 => conferido
(1 x 100,00 + 8 x 5,00   + 6 x 2,00   ) = 152,00 => conferido
(1 x 100,00 + 1 x 2,00   + 1 x 50,00  ) = 152,00 => conferido
(1 x 100,00 + 6 x 2,00   + 8 x 5,00   ) = 152,00 => conferido
(2 x 50,00  + 8 x 5,00   + 6 x 2,00   ) = 152,00 => conferido
(2 x 50,00  + 6 x 2,00   + 8 x 5,00   ) = 152,00 => conferido

